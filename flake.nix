{
  description = "haskell shell";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        with nixpkgs.legacyPackages.${system};
        {
          devShells.default =
            mkShellNoCC {
              packages =
                [
                  ghc
                  cabal-install
                  haskellPackages.cabal-fmt
                  ghcid
                  ormolu
                  haskell-language-server
                ];
            };
        }
      );
}
